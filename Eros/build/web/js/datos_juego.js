/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function iniciarJuego(msg) {
    jugador = msg.jugador;
    idJugadorEnTurno = msg.idJugadorEnTurno;
    nombres = msg.nombres;
    puntajes = msg.puntajes;
    enunciadoRespuestas = msg.enunciadoRespuestas;
    enunciadoPregunta = msg.enunciadoPregunta;

    //se muestran las preguntas 
    if (jugador.id == idJugadorEnTurno) {
        document.getElementById("nombrejugador").innerText = jugador.nickname;
        document.getElementById("puntjugador").innerText = jugador.puntaje;
        document.getElementById("juego").style.display = "inline";
        document.getElementById("salaEspera").style.display = "none";
        
          //tabla de posciones dentro del juego
        var codigo_en_juego = "";
        var codigo_progreso_jugador = "";
        var cont = 0
        for (i in nombres) {
            codigo_en_juego += '<a class="list-group-item list-group-item-action disabled">'+nombres[i]+' '+puntajes[i]+'</a>';
            codigo_progreso_jugador = '<div class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '+cont+'%;"></div>'
            cont +=20
        };
        document.getElementById("posicionesJuego").innerHTML = codigo_en_juego;
        document.getElementById("progresoJugador").innerHTML = codigo_progreso_jugador;
        //En el caso de que haya recibido una pregunta
        if (enunciadoPregunta != "") {
            var inyeccion_respuestas = "";
            for (i in enunciadoRespuestas) {
                var id = parseInt(i) + 1;
                //se debe cambiar la logica de esta linea, el value ahora es la respuesta misma
                inyeccion_respuestas += '<input type="button" class="btn btn-outline-success" value="'+enunciadoRespuestas[i]+'">';
            };
            document.getElementById("vista_opciones_respuesta").innerHTML = inyeccion_respuestas;
            document.getElementById("vista_enunciado_pregunta").innerHTML = enunciadoPregunta;
        }
    } else {
        document.getElementById("titulo").innerText = "El juego ha iniciado!!!";
        document.getElementById("texto").innerText = "Turno de " + nombres[idJugadorEnTurno - 1];
        document.getElementById("juego").style.display = "none";
        document.getElementById("salaEspera").style.display = "inline";
        var codigo_sala_espera = "";
        for (i in nombres) {
            codigo_sala_espera += '<tr class="table-dark"><td> <p>' + nombres[i] + '</p> </td> <td> <p>' + puntajes[i] + '</p> </td></tr>'
        }
        document.getElementById("contenidoPuntajes").innerHTML = codigo_sala_espera;
        document.getElementById("tablaPuntajes").style.display = "inline";
    }
    // Remover el código que indica que está esperando a que se unan los
    // jugadores.

    // Acá se debe mirar si el jugador está en turno para saber si se debe
    // inyectar el código de la sala de espera o el de la ruleta.

    //jugador = msg.jugador;
    //idJugadorEnTurno = msg.idJugadorEnTurno;
    //informacionJugadores = msg.informacionJugadores;
    //implementar toda la lógica del mensaje! OJO QUE ACÁ SE ASUME QUE LLEGA EN JSON
    //inyectar();

}