/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var arrselecion = 0;
$(function () {
    const wheel = document.querySelector('.wheel');
    const startButton = document.querySelector('.button');
    let deg = 0;
    startButton.addEventListener('click', () => {
        startButton.style.pointerEvents = 'none';
        deg = Math.floor(5000 + Math.random() * 5000);
        wheel.style.transition = 'all 10s ease-out';
        wheel.style.transform = `rotate(${deg}deg)`;
        wheel.classList.add('blur');
    });
    wheel.addEventListener('transitionend', () => {
        wheel.classList.remove('blur');
        startButton.style.pointerEvents = 'auto';
        wheel.style.transition = 'none';
        const actualDeg = deg % 360;
        //console.log(actualDeg)
        wheel.style.transform = `rotate(${actualDeg}deg)`;
        if ((actualDeg >= 331 && actualDeg <= 359) || (actualDeg >= 0 && actualDeg <= 30)) {
            arrselecion = 1
        } else if (actualDeg >= 31 && actualDeg <= 90) {
            arrselecion = 2
        } else if (actualDeg >= 91 && actualDeg <= 150) {
            arrselecion = 3
        } else if (actualDeg >= 151 && actualDeg <= 210) {
            arrselecion = 4
        } else if (actualDeg >= 211 && actualDeg <= 270) {
            arrselecion = 5
        } else if (actualDeg >= 271 && actualDeg <= 330) {
            arrselecion = 6
        }
        toBackend()
        document.getElementById("ruleta").style.display = "none";
    });
});
function toBackend() {//Otra funcion              
    //dataToSend = tomarDatosJugador();
    //alert(arrselecion)
    dataToSend = JSON.stringify(arrselecion);
    $.ajax({
        url: "ServletPreguntas",
        contentType: "application/json; charset=UTF-8",
        dataType: "JSON",
        type: "POST",
        data: dataToSend,
        success: function (result) {
            alert("Registro Exitoso");
        },
    });
};


