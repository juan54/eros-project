/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var websocket;


var _WEBSOCKETS_SERVER_URL = "ws://" + document.location.hostname + ":8080/Eros/";

var publicaciones, usuario

function connectWS() {
    // notar el protocolo.. es 'ws' y no 'http'
    var wsUri = _WEBSOCKETS_SERVER_URL + "perrito/";
    websocket = new WebSocket(wsUri); // creamos el socket
    websocket.onopen = function (evt) { // manejamos los eventos...
        console.log("Conectado..."); // ... y aparecerá en la pantalla
    };

    // Todo lo que llega del servidor !!!!!
    websocket.onmessage = function (evt) { // cuando se recibe un mensaje
        let msg = evt.data;
        msg = JSON.parse(msg);
        console.log(msg);
        publicaciones = msg.publicaciones
        usuario = msg.usuario
        inyeccion_publicacion = "";
        inyeccion_comentarios = "";
        for (var i=publicaciones.length-1; i>=0; i--) {
            inyeccion_publicacion += '<br><div class="container"> <h2>'+publicaciones[i].titulo+
            '</h2> <h5>'+publicaciones[i].nicknameUsuario+' '+publicaciones[i].fecha+'</h5> <p>' +publicaciones[i].texto+ '</p><div class="container" id="comentario_usuario3"></div>' +
            '<div class="form-group"><label for="exampleTextarea">Comentar</label><textarea class="form-control" id="area_comm_usr3"></textarea>' +
            '<button class="btn btn-primary" id="replica_usr3" onclick="replicar()">Replicar</button></div></div>';
            for (var j in publicaciones[i].comentarios) {
                inyeccion_comentarios += '<br><div class="container"><h6>'+publicaciones[i].comentarios[j].nicknameUsuario+', '+publicaciones[i].comentarios[j].fecha+'</h6> <p>' +publicaciones[i].comentarios[j].texto+ '</p><div class="container" id="comentario_usuario3"></div>';
            }
        }
        $("#muro_publicaciones").empty()
        $("#muro_publicaciones").append(inyeccion_publicacion)
    };

    websocket.onerror = function (evt) {
        console.log("oho!.. error:" + evt.data);
    };

    websocket.onclose = function (evt) {
        console.log(evt);

    };


}

function Publicaciones() {
    var Titulo = $("#titulo_publ").val();
    var Texto = $("#cuerpo_publ").val();
    //var NickUsuario = $("#genero").val();
    var Fecha = new Date();
    if (Titulo !== "" && Texto !== "") {
        publicacion = {
            id: 0,
            titulo: Titulo,
            texto: Texto,
            nicknameUsuario: usuario.nickname,
            fecha: Fecha,
            comentarios: null
        };
        enviarMensaje(JSON.stringify(publicacion))
    }
    ;
}
;

function enviarMensaje(text) {
    websocket.send(text);
    console.log("Enviando:" + text);
}

$(function () {
    $('#hacerPublicacion').click(function () {
        Publicaciones();
    });
});

connectWS()
