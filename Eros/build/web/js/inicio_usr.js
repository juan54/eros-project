
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

var Usuario = {

    nickname: "",
    id: "",
    correo: "",
    edad: "",
    genero: "",
    conectado: false,
    crearUsuario(evt) {

        msg = evt.data;
        msg = JSON.parse(evt);
        this.nickname = msg.nickname;
        this.correo = msg.correo;
        this.edad = msg.edad;
        this.genero = msg.genero;
        this.id=msg.id;
        this.conectado = true;
        
    },
    sesion() {
        return this.conectado;
    }
}
function verificarSesion() {

    return Usuario.sesion();
}

function tomarDatosInicio() {
    var nick = $("#uname").val();
    var contraseña = $("#pwd").val();

    if (nick != "" && contraseña != "") {
        alert("pollo")
        usuario = {
            nickname: nick,
            contraseña: contraseña
        }
        dataToSend = JSON.stringify(usuario);

        return dataToSend;
    }
    ;
}
;

function tomarDatosRegistro() {
    var nick = $("#uname1").val();
    var contraseña = $("#pwd1").val();
    var correo = $("#email").val();
    var genero = $("#gen").val();
    var edad = $("#edad").val();

    var hash = CryptoJS.SHA1(contraseña);

    if (nick != "" && hash != "" &&
            correo != "" && genero != ""
            && edad != "") {
        usuario = {
            nickname: nick,
            contraseña: hash.toString(),
            correo: correo,
            edad: edad,
            genero: genero
        }
        dataToSend = JSON.stringify(usuario);

        return dataToSend;
    }
    ;
}
;

$(function () {
    $('#enviarRegistro').click(function () {
        enviarDatos(tomarDatosRegistro());
    });
});

$(function () {
    $('#enviarInicio').click(function (e) {
        e.preventDefault();
        enviarDatos(tomarDatosInicio());

    });
});

$(function () {
    $('#cerrarSesion').click(function (e) {
        e.preventDefault();
        enviarDatos(tomarDatosInicio());

    });
});


$(function () {
        
    $('#logros').click(function () {
        alert(verificarSesion());
        if (verificarSesion()) {
            $("#cuerpoModal").empty();
            var m = "<h3> Nickname \
                            </h3>\n\
        <p>Logrossssss </p>\n\
             <br>\
            <h3> Correo\
                            </h3>\
        <p>" + Usuario.correo + "</p>\n\
        <br>\
"
            $("#cuerpoModal").append(m);

        }
    });

});

$(function () {


    $('#perfil').click(function () {
        
        if (verificarSesion()) {
            $("#cuerpoModal").empty();
            var m = "<h3> Nickname \
                            </h3>\n\
        <p>" + Usuario.nickname + "</p>\n\
             <br>\
            <h3> Correo\
                            </h3>\
        <p>" + Usuario.correo + "</p>\n\
        <br>\
"
            $("#cuerpoModal").append(m);

        }
    });

});

function enviarDatos(dataToSend) {

    //ocultarElementos();
    //vistaJugador()
    $.ajax({
        url: "RecepcionDatos",
        contentType: "application/json; charset=UTF-8",
        dataType: "JSON",
        type: "POST",
        data: dataToSend,
        success: function (result) {
            Usuario.crearUsuario(result);
            iniciarSesion();
        },
        error: function () {
            alert("Ocurrio algun error inesperado ! ")
        }

    });

}

function iniciarSesion() {

    document.getElementById("botonSesion").style.display = "none";
    document.getElementById("vista_perfil").style.display = "inline";
    document.getElementById("cerrarSesion").style.display = "inline";
    document.getElementById("registrarSesion").style.display = "none";
    inyectar();


}

function inyectar() {
    if (verificarSesion()) {
        var m = "<h3> Nickname \
                            </h3>\n\
        <p>" + Usuario.nickname + "</p>\n\
             <br>\
            <h3> Correo\
                            </h3>\
        <p>" + Usuario.correo + "</p>\n\
        <br>\
"
        $("#cuerpoModal").append(m);
    }
}

function enviarUsuarioForo(dataToSend) {

    //ocultarElementos();
    //vistaJugador()
    $.ajax({
        url: "RecepcionUsuario",
        contentType: "application/json; charset=UTF-8",
        dataType: "JSON",
        type: "POST",
        data: dataToSend,
        success: function (result) {
            
            //Usuario.crearUsuario(result);
            //iniciarSesion();
        },
        error: function () {
            alert("Ocurrio algun error inesperado ! ")
        }

    });

}

$(function () {
    $('#btnForo').click(function () {
        if(verificarSesion()){
            enviarUsuarioForo(JSON.stringify(Usuario))
            window.location.replace("foro1.html")
        }else{
            alert("Inicie Sesión por favor.")
        }
    });
});







