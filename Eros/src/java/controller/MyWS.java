
package controller;
//codigo servidor
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Envio;
import modelo.Juego;
import modelo.Jugador;
import modelo.Pregunta;

@ApplicationScoped
@ServerEndpoint(value = "/pajarito/")
public class MyWS{

    public  static final ArrayList<MyWS> conexiones =new ArrayList<>();
    private Session sesionWS;
    private static final AtomicInteger idConexion= new AtomicInteger(0);
    private static final int nJugadores=2;
    private Jugador jugador;
    //private Envio envio;

    public MyWS() {
        idConexion.getAndIncrement();
    }
    
    
    @OnOpen
    public void onOpen(Session session) {
        if(!conexionesCompletas()){
            System.out.println("Creando Jugador y conexion");
            crearJugador();
            System.out.println("Hay "+Juego.jugadores.size()+" jugadores");
            this.sesionWS=session;
            conexiones.add(this);
        }
        if(conexionesCompletas()){
            System.out.println("Las conexiones están completas!");
            iniciarJuego();
            publicarGlobalmente();
            System.out.println(Juego.jugadores.get(0).getNickname());
            System.out.println(Juego.jugadores.get(1).getNickname());
            System.out.println(Juego.jugadores.get(2).getNickname());
            System.out.println(Juego.jugadores.get(3).getNickname());
        }
    }

    @OnError
    public void onError(Throwable t) {
    }

    @OnClose
    public void onClose() {
        conexiones.remove(this);
        Juego.jugadores.remove(jugador);
        System.out.println(jugador.getNickname()+" ha abandonado la partida.");
        //Si se vuelve a iniciar el juego las variables se reestablecen
        if(conexiones.isEmpty()){
           // Juego.reiniciar();
            idConexion.set(0);
            // Se vacía el arrayList de categorias
            Juego.categorias.clear();
            Juego.preguntaActual= new Pregunta();
        }
        
    }

    @OnMessage
    public static void onMessage(String texto) {
        System.out.println(texto);
        Jugador jRecibido = (Jugador)Utils.fromJson(texto, Jugador.class);
        System.out.println("El objeto se ha creado...");
        Juego.calificarRespuesta(jRecibido.getIdSeleccion());
        Juego.preguntaActual=new Pregunta();
        Juego.siguienteTurno();
        //Convertir el parámetro texto a objeto Jugador
        //Verificar la respuesta y calificar
        //Pasar al siguiente turno
        //Compara la respuesta del jugador en turno con la respuesta de la pregunta actual
        //Jugador jugador = (Jugador)Utils.fromJson(texto, Jugador.class);
        //Juego.calificarRespuesta(jugador.getSeleccion());
        //El onMessage por el momento solo le pasará información a todos los jugadores
        publicarGlobalmente();
    }
    
        public void iniciarJuego(){
            // Se selecciona al jugador en turno
            int nAleatorio = (int) Math.floor(Math.random()*(nJugadores));
            Juego.jugadorEnTurno=Juego.jugadores.get(nAleatorio);
            System.out.println("Turno de "+Juego.jugadorEnTurno.getNickname());
            // Se llena la información de las preguntas y respuestas
            Juego.crearInfo();
       }
           
       public static boolean conexionesCompletas(){
           System.out.println(conexiones.size()==nJugadores);
        return conexiones.size()==nJugadores;     
       }
       //Se necesita un servlet que reciba la informacion del jugador y la actualice por el metodo Juego.convertirUsuario en Juego.jugadorRecibido
       public void crearJugador(){
           System.out.println("Se va a intentar crear al jugador...");
        jugador =new Jugador(idConexion.intValue(),Juego.jugadorRecibido.getNickname(), Juego.jugadorRecibido.getEdad(), 
                Juego.jugadorRecibido.getGenero(), Juego.jugadorRecibido.getPuntaje(), Juego.jugadorRecibido.getIdSeleccion());
        System.out.println("...Creado correctamente");
        System.out.println("Se va a agregar al jugador con id "+jugador.getId()+" y nickname: "+jugador.getNickname());
        Juego.jugadores.add(jugador);
     
       }
       
        public static void publicarGlobalmente() {
        for(int i=0; i<nJugadores; i++){
            MyWS conexion=conexiones.get(i);
            if(conexion.sesionWS.isOpen()){
                try {
                    //Objeto que se va a enviar a todos los jugadores
                    //Se enviará los objetos de la lista de jugadores
                    ArrayList<String> alias= Juego.obtenerNicknames();
                    ArrayList<Double> puntos= Juego.obtenerPuntajes();
                    System.out.println("Voy a conseguir las respuestas...");
                    ArrayList<String> respuestas= Juego.obtenerEnunciadosRespuesta();
                    System.out.println("Voy a conseguir la pregunta...");
                    String pregunta=Juego.preguntaActual.getEnunciado();
                    System.out.println("Voy a crear el objeto envio...");
                    Envio envio=new Envio(Juego.jugadores.get(i), Juego.jugadorEnTurno.getId(), alias, puntos, pregunta, respuestas);
                    System.out.println("Voy a convertirlo a Json...");
                    conexion.sesionWS.getBasicRemote().sendText(Utils.toJson(envio));
                } catch (IOException ex) {
                    Logger.getLogger(MyWS.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
     
    
}
