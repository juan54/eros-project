/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

/**
 *
 * @author Jofre Gavidia ☺☺
 */
public class Usuario {
    
    private int id;
    private String nickname;
    private String correo;
    private int edad;
    private char genero;
    private String contraseña; 

    public Usuario(int id, String nickname, String correo, int edad, char genero) {
        this.id = id;
        this.nickname = nickname;
        this.correo = correo;
        this.edad = edad;
        this.genero = genero;
    }
    
    public Usuario() {
    this.id = 0;
    this.nickname = "";
    this.correo = "";
    this.edad = 0;
    this.genero = 'M';
    this.contraseña="";
   }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }
    
    
    
}
