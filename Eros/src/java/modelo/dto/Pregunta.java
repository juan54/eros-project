/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

import java.util.ArrayList;

/**
 *
 * @author LENOVO
 */
public class Pregunta {
    private int id;
    private String categoria;
    private String enunciado;
    private ArrayList<Respuesta> opcionesRespuesta;
    
    public Pregunta(){
        this.id=0;
        this.enunciado=" ";
        this.opcionesRespuesta=new ArrayList<>();
    }
    
    public Pregunta(int id, String enunciado, ArrayList<Respuesta> respuestas){
        this.id=id;
        this.enunciado=enunciado;
        this.opcionesRespuesta=respuestas;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public ArrayList<Respuesta> getOpcionesRespuesta() {
        return opcionesRespuesta;
    }

    public void setOpcionesRespuesta(ArrayList<Respuesta> opcionesRespuesta) {
        this.opcionesRespuesta = opcionesRespuesta;
    }
    
}
