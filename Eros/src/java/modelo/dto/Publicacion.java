/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author LENOVO
 */
public class Publicacion {
    private int id;
    private String titulo;
    private String texto;
    private Date fecha;
    private String nicknameUsuario;
    private ArrayList<Publicacion> comentarios;

    public Publicacion() {
        this.id=0;
        this.titulo = "";
        this.texto = "";
        this.fecha = new Date();
        this.nicknameUsuario = "";
        this.comentarios= new ArrayList();
    }

    public Publicacion(int id, String titulo, String texto, Date fecha, String nicknameUsuario, ArrayList<Publicacion> comentarios) {
        this.id = id;
        this.titulo = titulo;
        this.texto = texto;
        this.fecha = fecha;
        this.nicknameUsuario = nicknameUsuario;
        this.comentarios = comentarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Publicacion> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Publicacion> comentarios) {
        this.comentarios = comentarios;
    }
    
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNicknameUsuario() {
        return nicknameUsuario;
    }

    public void setNicknameUsuario(String nicknameUsuario) {
        this.nicknameUsuario = nicknameUsuario;
    }
    
    
}
