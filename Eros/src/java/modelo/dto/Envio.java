/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

import java.util.ArrayList;
import modelo.dto.UsuarioJuego;


/**
 *
 * @author LENOVO
 */
public class Envio {
    private UsuarioJuego jugador;
    private int idJugadorEnTurno;
    private ArrayList<String> nombres;
    private ArrayList<Double> puntajes;
    private String enunciadoPregunta;
    private ArrayList<String> enunciadoRespuestas;

    public Envio(UsuarioJuego jugador, int idJugadorEnTurno, ArrayList<String> nombres, ArrayList<Double> puntajes, String enunciadoPregunta, ArrayList<String> enunciadoRespuestas) {
        this.jugador = jugador;
        this.idJugadorEnTurno = idJugadorEnTurno;
        this.nombres = nombres;
        this.puntajes = puntajes;
        this.enunciadoPregunta = enunciadoPregunta;
        this.enunciadoRespuestas = enunciadoRespuestas;
    }
    

    public UsuarioJuego getJugador() {
        return jugador;
    }

    public void setJugador(UsuarioJuego jugador) {
        this.jugador = jugador;
    }

    public int getIdJugadorEnTurno() {
        return idJugadorEnTurno;
    }

    public void setIdJugadorEnTurno(int idJugadorEnTurno) {
        this.idJugadorEnTurno = idJugadorEnTurno;
    }

    public ArrayList<String> getNombres() {
        return nombres;
    }

    public void setNombres(ArrayList<String> nombres) {
        this.nombres = nombres;
    }

    public ArrayList<Double> getPuntajes() {
        return puntajes;
    }

    public void setPuntajes(ArrayList<Double> puntajes) {
        this.puntajes = puntajes;
    }
  
}
