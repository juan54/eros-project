/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

/**
 *
 * @author LENOVO
 */
public class UsuarioJuego extends Usuario {

    private int idUsuarioJuego;
    private double puntaje;
    private int idSeleccion;

    public UsuarioJuego(int idUsuarioJuego, double puntaje, int idSeleccion,Usuario usuario) {
        super(usuario.getId(), usuario.getNickname(), usuario.getNickname(), usuario.getEdad(), usuario.getGenero());
        this.idUsuarioJuego = idUsuarioJuego;
        this.puntaje = puntaje;
        this.idSeleccion = idSeleccion;
    }

    public UsuarioJuego(int idUsuarioJuego, double puntaje, int idSeleccion) {
        this.idUsuarioJuego = idUsuarioJuego;
        this.puntaje = puntaje;
        this.idSeleccion = idSeleccion;
    }

   
    
    public UsuarioJuego() {
        super();
        this.idUsuarioJuego = 0;
        this.puntaje = 0;
        this.idSeleccion = 0;
    }
    
    
    public int getIdUsuarioJuego() {
        return idUsuarioJuego;
    }

    public void setIdUsuarioJuego(int idUsuarioJuego) {
        this.idUsuarioJuego = idUsuarioJuego;
    }

    public double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(double puntaje) {
        this.puntaje = puntaje;
    }

    public int getIdSeleccion() {
        return idSeleccion;
    }

    public void setIdSeleccion(int idSeleccion) {
        this.idSeleccion = idSeleccion;
    }


}
