/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

import java.util.ArrayList;

/**
 *
 * @author LENOVO
 */
public class EnvioForo {
    
    private Usuario usuario;
    private ArrayList<Publicacion> publicaciones;

    public EnvioForo(Usuario usuario, ArrayList<Publicacion> publicaciones) {
        this.usuario = usuario;
        this.publicaciones = publicaciones;
    }

    public EnvioForo() {
        this.usuario = new Usuario();
        this.publicaciones = new ArrayList();
    }
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ArrayList<Publicacion> getPublicaciones() {
        return publicaciones;
    }

    public void setPublicaciones(ArrayList<Publicacion> publicaciones) {
        this.publicaciones = publicaciones;
    }
    
    
}
