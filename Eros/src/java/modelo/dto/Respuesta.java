/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

/**
 *
 * @author LENOVO
 */
public class Respuesta {
    private int id;
    private String texto;
    private boolean esCorrecta;
    private String retroalimentacion;

    public String getRetroalimentacion() {
        return retroalimentacion;
    }

    public void setRetroalimentacion(String retroalimentacion) {
        this.retroalimentacion = retroalimentacion;
    }

    public Respuesta(){
        this.id=0;
        this.texto=" ";
        this.esCorrecta=false;
    }
    
    public Respuesta(int id, String texto, boolean esCorrecta){
        this.id=id;
        this.texto=texto;
        this.esCorrecta=esCorrecta;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean isEsCorrecta() {
        return esCorrecta;
    }

    public void setEsCorrecta(boolean esCorrecta) {
        this.esCorrecta = esCorrecta;
    }
    
    
}
