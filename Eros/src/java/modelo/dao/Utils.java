package modelo.dao;

import java.io.BufferedReader;
import javax.servlet.http.HttpServletRequest;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import modelo.dto.Envio;
import modelo.dao.Juego;
import modelo.dto.EnvioForo;
import modelo.dto.Usuario;
import modelo.dto.UsuarioJuego;

public class Utils {

    public static String readParams(HttpServletRequest request) {
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            request.setCharacterEncoding("UTF-8");
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
        } catch (Exception e) {
            /* report an error */ }

        line = jb.toString();
        return line;
    }

    /**
     * FUNCIONES JSON
     */
    public static GsonBuilder builder = null;

    public static String toJson(Juego obj) {
        builder = new GsonBuilder();
        builder.setExclusionStrategies(new TestExclStrat());
        return builder.create().toJson(obj);

    }

    public static String toJson(String obj) {
        builder = new GsonBuilder();
        builder.setExclusionStrategies(new TestExclStrat());
        return builder.create().toJson(obj);

    }
    
    public static String toJson(Usuario obj) {
        builder = new GsonBuilder();
        builder.setExclusionStrategies(new TestExclStrat());
        return builder.create().toJson(obj);

    }
    

    public static String toJsonArrayList(ArrayList array) {
        String retorno = "";
        for (Object object : array) {
            builder = new GsonBuilder();
            builder.setExclusionStrategies(new TestExclStrat());
            retorno += builder.create().toJson(object);
        }
        return retorno;
    }

    public static <T> Object fromJson(String json, Class<T> obj) {

        builder = new GsonBuilder();
        builder.setExclusionStrategies(new TestExclStrat());

        return builder.create().fromJson(json, obj);
    }

    public static String toJson(EnvioForo envio) {
        builder = new GsonBuilder();
        builder.setExclusionStrategies(new TestExclStrat());
        return builder.create().toJson(envio);
    }

    @Deprecated
    public static class TestExclStrat implements ExclusionStrategy {

        public boolean shouldSkipClass(Class<?> arg0) {
            return false;
        }

        public boolean shouldSkipField(FieldAttributes f) {

            return false;
        }
    }
}
