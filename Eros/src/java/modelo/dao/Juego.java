/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import modelo.dto.Categoria;
import modelo.dto.Pregunta;
import modelo.dto.Respuesta;
import modelo.dto.UsuarioJuego;

/**
 *
 * @author LENOVO
 */
public class Juego {
    public static ArrayList<Categoria> categorias = new ArrayList<>();
    public static Pregunta preguntaActual = new Pregunta();
    public static ArrayList<UsuarioJuego> jugadores =new ArrayList<>();
    public static UsuarioJuego jugadorEnTurno;
    public static UsuarioJuego jugadorRecibido;
    
    public static void crearInfo(){
        String C1="Historia";
        String C2="Afectivo";
        String C3="Ciencia";
        String C4="Biologia";
        String C5="Cultural";
        String C6="Social";
        
        int idR=0;
        
        ArrayList<Respuesta> RP1C1 = new ArrayList<Respuesta>();
        ArrayList<Respuesta> RP1C2 = new ArrayList<Respuesta>();
        ArrayList<Respuesta> RP1C3 = new ArrayList<Respuesta>();
        ArrayList<Respuesta> RP1C4 = new ArrayList<Respuesta>();
        ArrayList<Respuesta> RP1C5 = new ArrayList<Respuesta>();
        ArrayList<Respuesta> RP1C6 = new ArrayList<Respuesta>();
        Respuesta R1P1C1=new Respuesta(1, "Sigmund Freud (1856-1939)", true);
        RP1C1.add(R1P1C1);
        Respuesta R2P1C1=new Respuesta(2, "Diego Armando Maradona", false);
        RP1C1.add(R2P1C1);
        Respuesta R3P1C1=new Respuesta(3, "Aristoteles", false);
        RP1C1.add(R3P1C1);
        Respuesta R1P1C2=new Respuesta(1, "Lo ideal es que hayas planificado ese primer encuentro sexual; ¿con quién lo vas a tener?, , ¿dónde lo van a hacer?, ¿cómo se van a proteger?.", true);
        RP1C2.add(R1P1C2);
        Respuesta R2P1C2=new Respuesta(2, "A los 16", false);
        RP1C2.add(R2P1C2);
        Respuesta R3P1C2=new Respuesta(3, "Virgen hasta el matrimonio", false);
        RP1C2.add(R3P1C2);
        Respuesta R1P1C3=new Respuesta(1, "Si, pero le toca esforzarse el doble", true);
        RP1C3.add(R1P1C3);
        Respuesta R2P1C3=new Respuesta(2, "Si, pero sale a la mitad", false);
        RP1C3.add(R2P1C3);
        Respuesta R3P1C3=new Respuesta(3, "No, uno es igual a dos", false);
        RP1C3.add(R3P1C3);
        Respuesta R1P1C4=new Respuesta(1, "Sí, se puede fracturar. Si bien el pene no tiene hueso, cuando está erecto puede fracturarse la túnica albugínea", true);
        RP1C4.add(R1P1C4);
        Respuesta R2P1C4=new Respuesta(2, "No, obvio no", false);
        RP1C4.add(R2P1C4);
        Respuesta R3P1C4=new Respuesta(3, "Si, pero puede salir otro", false);
        RP1C4.add(R3P1C4);
        Respuesta R1P1C5=new Respuesta(1, "Sí, incluso la ciencia dice que existe como una de las orientaciones sexuales del ser humano.", true);
        RP1C5.add(R1P1C5);
        Respuesta R2P1C5=new Respuesta(2, "No, eso no existe", false);
        RP1C5.add(R2P1C5);
        Respuesta R3P1C5=new Respuesta(3, "No, para nada", false);
        RP1C5.add(R3P1C5);
        Respuesta R1P1C6=new Respuesta(1, "Una persona Trans es aquella que no se identifica con el género asignado al nacer", true);
        RP1C6.add(R1P1C6);
        Respuesta R2P1C6=new Respuesta(2, "Una persona que le gusta el TRANSmilenio.", false);
        RP1C6.add(R2P1C6);
        Respuesta R3P1C6=new Respuesta(3, "No sabe no responde.", false);
        RP1C6.add(R2P1C6);
        
        Pregunta P1C1= new Pregunta(1, "Medico y psicoanalista vienés que demostró la trascendencia de la sexualidad en los individuos.", RP1C1);
        Pregunta P1C2= new Pregunta(2, "¿A qué edad es recomendado tener sexo?", RP1C2);
        Pregunta P1C3= new Pregunta(3, "¿Se puede tener hijos con un solo testículo?", RP1C3);
        Pregunta P1C4= new Pregunta(4, "¿Se puede fracturar el pene?", RP1C4);
        Pregunta P1C5= new Pregunta(5, "¿Existe realmente la bisexualidad?", RP1C5);
        Pregunta P1C6= new Pregunta(6, "¿Qué es ser transexual?", RP1C6);
        
        ArrayList<Pregunta> PC1 = new ArrayList<Pregunta>();
        ArrayList<Pregunta> PC2 = new ArrayList<Pregunta>();
        ArrayList<Pregunta> PC3 = new ArrayList<Pregunta>();
        ArrayList<Pregunta> PC4 = new ArrayList<Pregunta>();
        ArrayList<Pregunta> PC5 = new ArrayList<Pregunta>();
        ArrayList<Pregunta> PC6 = new ArrayList<Pregunta>();
        
        PC1.add(P1C1);
        PC2.add(P1C2);
        PC3.add(P1C3);
        PC4.add(P1C4);
        PC5.add(P1C5);
        PC6.add(P1C6);
        
        Categoria CA1= new Categoria(C1, 1, PC1);
        Categoria CA2= new Categoria(C2, 2, PC2);
        Categoria CA3= new Categoria(C3, 3, PC3);
        Categoria CA4= new Categoria(C4, 4, PC4);
        Categoria CA5= new Categoria(C5, 5, PC5);
        Categoria CA6= new Categoria(C6, 6, PC6);
        
        categorias.add(CA1);
        categorias.add(CA2);
        categorias.add(CA3);
        categorias.add(CA4);
        categorias.add(CA5);
        categorias.add(CA6);
        
    }
    
    public static void asignarJugador(UsuarioJuego jugador){
        
        jugadorEnTurno=jugador;
    }
    
    public static void encontrarPregunta(int idCategoria){
        
        for(Categoria categoria : categorias){
            System.out.println("Comparando "+idCategoria+" con "+categoria.getId());
            if(categoria.getId()==idCategoria){
                 //Collections.shuffle(categoria.getPreguntas());
                 preguntaActual=categoria.getPreguntas().get(0);
                 break;
            }
            
        }
          
    }
    
    public static void calificarRespuesta(int idRespuesta){
        
        for(Respuesta opcion : preguntaActual.getOpcionesRespuesta()){
            if(opcion.getId()==idRespuesta){
                if(opcion.isEsCorrecta()){
                    jugadorEnTurno.setPuntaje(jugadorEnTurno.getPuntaje()+200);
                    jugadores.get(jugadorEnTurno.getId()-1).setPuntaje(jugadorEnTurno.getPuntaje()+200);
                }
            }
        }
        
    }
    
    
    public static UsuarioJuego convertirUsuario(String x) {
           jugadorRecibido = (UsuarioJuego)Utils.fromJson(x, UsuarioJuego.class);
           return jugadorRecibido;
    }
    
    public static ArrayList<String> obtenerNicknames(){
        ArrayList<String> nombres = new ArrayList();
        for(UsuarioJuego j : jugadores){
            nombres.add(j.getNickname());
        }
        return nombres;
    }
    
    public static ArrayList<Double> obtenerPuntajes(){
        ArrayList<Double> puntajes = new ArrayList();
        for(UsuarioJuego j : jugadores){
            puntajes.add(j.getPuntaje());
        }
        return puntajes;
    }
    
    public static ArrayList<String> obtenerEnunciadosRespuesta(){
        ArrayList<String> respuestas = new ArrayList();
        System.out.println("Voy a entrar al for...");
        for(Respuesta resp : preguntaActual.getOpcionesRespuesta()){
            System.out.println("Voy a agregar un texto...");
            respuestas.add(resp.getTexto());
        }
        return respuestas;
    }
    
    public static void siguienteTurno(){
        //Se asume que los id comienzan en 1
        if(jugadorEnTurno.getId()==jugadores.size()){
            jugadorEnTurno=jugadores.get(0);
        }else{
             int idActual=jugadorEnTurno.getId();
             jugadorEnTurno=jugadores.get(idActual);
        }
    }
}
