/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import modelo.dto.Usuario;

/**
 *
 * @author Jofre Gavidia ☺☺
 */
public class AdminUsuario {

    private static Usuario miUsuario = new Usuario();
    private static ArrayList<Usuario> usuarios = new ArrayList<>();
    public static Queue<Usuario> colaPeticionesJuego = new LinkedList();
    public static int peticiones = 0;
    public static final int nPeticiones =2;

    public static Usuario iniciarSesion(Usuario usuario) {

        if (usuario.getNickname().equals("Luis") && usuario.getContraseña().equals("12345678")) {
            usuario.setNickname("Luis");
            usuario.setCorreo("luis@gmail.com");
            usuario.setGenero('M');
            usuario.setEdad(23);
            usuario.setId(1);
            miUsuario = usuario;
            usuarios.add(usuario);
            return usuario;
        }
        
        if (usuario.getNickname().equals("Carlos") && usuario.getContraseña().equals("12345678")) {
            usuario.setNickname("Carlos");
            usuario.setCorreo("carlos@gmail.com");
            usuario.setGenero('M');
            usuario.setEdad(23);
            usuario.setId(2);
            miUsuario = usuario;
            usuarios.add(usuario);
            return usuario;
        }

        return null;
    }

    public static Usuario buscarUsuario(int id) {

        Usuario user = null;

        for (Usuario x : usuarios) {

            if (x.getId() == id) {

                user = x;
                return user;
            }

        }

        return null;
    }

    public static Usuario cerrarSesion(int id) {

        return usuarios.remove(usuarios.indexOf(buscarUsuario(id)));
    }

    public static boolean verificarPeticiones() {

        return peticiones <= nPeticiones;
    }

    public static void recibirPeticion(int id) {
        
        

        if (verificarPeticiones()) {

            colaPeticionesJuego.add(usuarios.get(usuarios.indexOf(buscarUsuario(id))));
            peticiones++;
            System.out.println("guardando petición numero ... "+peticiones);
        }

    }

    public static void registrar(Usuario usuario) {

    }

    public static Usuario convertirUsuario(String datos) {

        return null;
    }

    public static Usuario datosUsuarioJuego() {

        return usuarios.get(usuarios.indexOf(miUsuario));
    }

}
