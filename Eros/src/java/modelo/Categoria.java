/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Jofre Gavidia ☺☺
 */
public class Categoria {
    
    private String nombre;
    private int id;
    private ArrayList<Pregunta> preguntas;

    public Categoria(String nombre, int id, ArrayList<Pregunta> preguntas) {
        this.nombre = nombre;
        this.id = id;
        this.preguntas = preguntas;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(ArrayList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
  
    
}
