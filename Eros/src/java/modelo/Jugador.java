/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author LENOVO
 */
public class Jugador {
    private int id;
    private String nickname;
    private int edad;
    private char genero;
    private double puntaje;
    private int idSeleccion;
    
    public Jugador(){
        this.id=0;
        this.nickname=" ";
        this.edad=0;
        this.genero=' ';
        this.puntaje=0;
        this.idSeleccion=0;
    }
    
    public Jugador(int id, String nickname, int edad, char genero, double puntaje, int idSeleccion){
        this.id=id;
        this.nickname=nickname;
        this.edad=edad;
        this.genero=genero;
        this.puntaje=puntaje;
        this.idSeleccion=idSeleccion;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(double puntaje) {
        this.puntaje = puntaje;
    }

    public int getIdSeleccion() {
        return idSeleccion;
    }

    public void setIdSeleccion(int idSeleccion) {
        this.idSeleccion = idSeleccion;
    }

    
    
}
