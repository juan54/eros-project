/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladorBlog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import modelo.dao.Utils;
import modelo.dto.Envio;
import modelo.dto.EnvioForo;
import modelo.dto.Usuario;
import modelo.dto.Foro;
import modelo.dto.Publicacion;

/**
 *
 * @author Jofre Gavidia ☺☺
 */

@ApplicationScoped
@ServerEndpoint(value = "/perrito/")
public class WSForo {
    private Usuario usuario;
    private Session sesionWS;
    private static final AtomicInteger idConexion = new AtomicInteger(0);

    public WSForo() {
        idConexion.getAndIncrement();
        System.out.println(idConexion);
    }

    @OnOpen
    public void OnOpen(Session session) {
        usuario=Foro.usuarioRecibido;
        // LEER PUBLICACIONES DE LA BASE DE DATOS E IGUALARLO AL ARRAY PUBLICACIONES
        Foro.crearPublicaciones();
        this.sesionWS = session;
        publicar();
    }

    @OnMessage
    public void OnMessage(String message, Session session) {
        System.out.println("Objeto recibido del foro: "+message);
        Publicacion publicacion= (Publicacion)Utils.fromJson(message, Publicacion.class);
        //ACTUALIZAR BASE DE DATOS
        Foro.publicaciones.add(publicacion);
        publicar();
    }

    @OnClose
    public void OnClose(Session session) {
        Foro.usuarioRecibido = usuario;
        System.out.println("Session closed ==>");
        //conexiones.remove(this);
    }

    @OnError
    public void OnError(Throwable e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
    }

    public void publicar() {

        if (sesionWS.isOpen()) {
            try {
                EnvioForo envio = new EnvioForo(usuario, Foro.publicaciones);
                sesionWS.getBasicRemote().sendText(Utils.toJson(envio));
            } catch (IOException ex) {
                Logger.getLogger(WSForo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
