package controladorJuego;
//codigo servidor

import modelo.dao.Utils;
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.scene.input.KeyCode.T;
import modelo.dao.AdminUsuario;
import modelo.dto.Envio;
import modelo.dao.Juego;
import modelo.dto.UsuarioJuego;
import modelo.dto.Pregunta;
import modelo.dto.Usuario;

@ApplicationScoped
@ServerEndpoint(value = "/pajarito/")
public class MyWS {

    public static final ArrayList<MyWS> conexiones = new ArrayList<>();
    private Session sesionWS;
    private static final AtomicInteger idConexion = new AtomicInteger(0);
    private static final int nJugadores = 2;
    private UsuarioJuego jugador;
    //private Envio envio;

    public MyWS() {
        idConexion.getAndIncrement();
    }

    @OnOpen
    public void onOpen(Session session) {

        for (Usuario user : AdminUsuario.colaPeticionesJuego) {

            System.out.println("Creando Jugador y conexion");
            crearJugador(user);
            System.out.println("Hay " + Juego.jugadores.size() + " jugadores");
            this.sesionWS = session;
            conexiones.add(this);
        }

        if (conexionesCompletas()) {
            System.out.println("Las conexiones están completas!");
            iniciarJuego();
            publicarGlobalmente();
            System.out.println(Juego.jugadores.get(0).getNickname());
            System.out.println(Juego.jugadores.get(1).getNickname());
            System.out.println(Juego.jugadores.get(2).getNickname());
            System.out.println(Juego.jugadores.get(3).getNickname());
        }
    }

    @OnError
    public void onError(Throwable t) {
    }

    @OnClose
    public void onClose() {
        conexiones.remove(this);
        Juego.jugadores.remove(jugador);
        System.out.println(jugador.getNickname() + " ha abandonado la partida.");
        //Si se vuelve a iniciar el juego las variables se reestablecen
        if (conexiones.isEmpty()) {
            // Juego.reiniciar();
            idConexion.set(0);
            // Se vacía el arrayList de categorias
            Juego.categorias.clear();
            Juego.preguntaActual = new Pregunta();
        }

    }

    @OnMessage
    public static void onMessage(String texto) {
        System.out.println(texto);
        UsuarioJuego jRecibido = (UsuarioJuego) Utils.fromJson(texto, UsuarioJuego.class);
        System.out.println("El objeto se ha creado...");
        Juego.calificarRespuesta(jRecibido.getIdSeleccion());
        Juego.preguntaActual = new Pregunta();
        Juego.siguienteTurno();
        //Convertir el parámetro texto a objeto Jugador
        //Verificar la respuesta y calificar
        //Pasar al siguiente turno
        //Compara la respuesta del jugador en turno con la respuesta de la pregunta actual
        //Jugador jugador = (Jugador)Utils.fromJson(texto, Jugador.class);
        //Juego.calificarRespuesta(jugador.getSeleccion());
        //El onMessage por el momento solo le pasará información a todos los jugadores
        publicarGlobalmente();
    }

    public void iniciarJuego() {
        // Se selecciona al jugador en turno
        int nAleatorio = (int) Math.floor(Math.random() * (nJugadores));
        Juego.jugadorEnTurno = Juego.jugadores.get(nAleatorio);
        System.out.println("Turno de " + Juego.jugadorEnTurno.getNickname());
        // Se llena la información de las preguntas y respuestas
        Juego.crearInfo();
    }

    public static boolean conexionesCompletas() {
        System.out.println(conexiones.size() == nJugadores);
        return conexiones.size() == nJugadores;
    }
    //Se necesita un servlet que reciba la informacion del jugador y la actualice por el metodo Juego.convertirUsuario en Juego.jugadorRecibido

    public void crearJugador(Usuario user) {
        jugador = new UsuarioJuego(idConexion.intValue(), 0, 0, user);
        System.out.println("...Creado correctamente");
        System.out.println("Se va a agregar al jugador con id de usuario : " + jugador.getId() + "id de juego : " + jugador.getIdUsuarioJuego() + "  y nickname: " + jugador.getNickname());
        Juego.jugadores.add(jugador);

    }

    public static void publicarGlobalmente() {
        for (int i = 0; i < nJugadores; i++) {
            MyWS conexion = conexiones.get(i);
            if (conexion.sesionWS.isOpen()) {
                try {
                    //Objeto que se va a enviar a todos los jugadores
                    //Se enviará los objetos de la lista de jugadores
                    ArrayList<String> alias = Juego.obtenerNicknames();
                    ArrayList<Double> puntos = Juego.obtenerPuntajes();
                    System.out.println("Voy a conseguir las respuestas...");
                    ArrayList<String> respuestas = Juego.obtenerEnunciadosRespuesta();
                    System.out.println("Voy a conseguir la pregunta...");
                    String pregunta = Juego.preguntaActual.getEnunciado();
                    System.out.println("Voy a crear el objeto envio...");
                    Envio envio = new Envio(Juego.jugadores.get(i), Juego.jugadorEnTurno.getId(), alias, puntos, pregunta, respuestas);
                    System.out.println("Voy a convertirlo a Json...");
                    conexion.sesionWS.getBasicRemote().sendText(Utils.toJson("jfsdljk"));
                } catch (IOException ex) {
                    Logger.getLogger(MyWS.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
