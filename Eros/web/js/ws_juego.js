/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//codigo cliente

var websocket;


var _WEBSOCKETS_SERVER_URL = "ws://" + document.location.hostname + ":8080/Eros/";


function connectWS() {
    // notar el protocolo.. es 'ws' y no 'http'
    var wsUri = _WEBSOCKETS_SERVER_URL + "pajarito/";
    websocket = new WebSocket(wsUri); // creamos el socket
    websocket.onopen = function (evt) { // manejamos los eventos...
        console.log("Conectado..."); // ... y aparecerá en la pantalla
    };

    // Todo lo que llega del servidor !!!!!
    websocket.onmessage = function (evt) { // cuando se recibe un mensaje

        let msg = evt.data;
        msg = JSON.parse(msg);
        console.log(msg);
        iniciarJuego(msg);
        //implementar toda la lógica del mensaje! OJO QUE ACÁ SE ASUME QUE LLEGA EN JSON

    };

    websocket.onerror = function (evt) {
        console.log("oho!.. error:" + evt.data);
    };

    websocket.onclose = function (evt) {
        console.log(evt);

    };


}



function enviarMensaje(text) {
    websocket.send(text);
    console.log("Enviando:" + text);
}


// El usuario (Previamente registrado) envia una petición para jugar.


$(function () {
    $('#juega').click(function (e) {
        e.preventDefault();
        if(verificarSesion()){
            dataToSend = JSON.stringify(Usuario.id)
            peticionJuego(dataToSend)
            
        }else{
            
            alert("inicie sesión ");
        }
        //peticionJuego(idUsuario);
        //sleep(500);
        //connectWS();

    });
});

function peticionJuego(dataToSend) {

    alert(dataToSend);
    //ocultarElementos();
    //vistaJugador()
    $.ajax({
        url: "PeticionesJuego",
        contentType: "application/json; charset=UTF-8",
        dataType: "JSON",
        type: "POST",
        data: dataToSend,
        success: function (result) {
            alert(result);
            msg = JSON.parse(result);
            
            if(msg=="true"){
                sleep(500);
                connectWS();
                window.location.replace("tabla_posiciones.html")
                
            }
            
        },
        error: function () {
            alert("Error, no ha podido unirse a una sala")
        }

    });

}

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}


