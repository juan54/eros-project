var websocket;

var _WEBSOCKETS_SERVER_URL = "ws://" + document.location.hostname + ":8080/Eros/";

var jugador, idJugadorEnTurno, nombres, puntajes, enunciadoRespuestas, enunciadoPregunta;

function comprobar() {
  respuesta_jugador = $("input[type=radio]:checked").val()
  jugador.idSeleccion=parseInt(respuesta_jugador)
  text = JSON.stringify(jugador);
  enviarMensaje(text)
};

function enviarMensaje(text) {
    console.log("Enviando:" + text);
    websocket.send(text);
}

function connectWS() {
    // notar el protocolo.. es 'ws' y no 'http'
    var wsUri = _WEBSOCKETS_SERVER_URL + "pajarito/";
    websocket = new WebSocket(wsUri); // creamos el socket

    websocket.onopen = function(evt) { // manejamos los eventos...
        console.log("Conectado..."); // ... y aparecerá en la pantalla
    };

    websocket.onmessage = function(evt) { // cuando se recibe un mensaje
        console.log("He recibido un mensaje...")
        let msg = evt.data;
        msg = JSON.parse(msg);
        console.log(msg);
        jugador=msg.jugador;
        idJugadorEnTurno=msg.idJugadorEnTurno;
        nombres = msg.nombres;
        puntajes=msg.puntajes;
        enunciadoRespuestas=msg.enunciadoRespuestas;
        enunciadoPregunta=msg.enunciadoPregunta;
        
        if(jugador.id==idJugadorEnTurno){
            document.getElementById("nombrejugador").innerText = jugador.nickname;
            document.getElementById("puntjugador").innerText = jugador.puntaje;
            document.getElementById("juego").style.display = "inline";
           document.getElementById("salaEspera").style.display = "none";
           //En el caso de que haya recibido una pregunta
           if(enunciadoPregunta!=""){
               var inyeccion_respuestas = "";
               for (i in enunciadoRespuestas){
                var id=parseInt(i)+1;
                inyeccion_respuestas += '<input type="radio" name="opciones" value="'+id+'"><label>'+enunciadoRespuestas[i]+'</label><br>'
               };
               document.getElementById("vista_opciones_respuesta").innerHTML = inyeccion_respuestas;
               document.getElementById("vista_enunciado_pregunta").innerHTML = enunciadoPregunta;
           }
        } else {
            document.getElementById("titulo").innerText = "El juego ha iniciado!!!";
            document.getElementById("texto").innerText = "Turno de "+nombres[idJugadorEnTurno-1];
            document.getElementById("juego").style.display = "none";
            document.getElementById("salaEspera").style.display = "inline";
            
            var codigo="";
            for(i in nombres){
                codigo+='<tr><td> <p>'+nombres[i]+'</p> </td> <td> <p>'+puntajes[i]+'</p> </td></tr>'
            }
            document.getElementById("contenidoPuntajes").innerHTML = codigo;
           document.getElementById("tablaPuntajes").style.display = "inline";
        }
        // Remover el código que indica que está esperando a que se unan los
        // jugadores.
        
        // Acá se debe mirar si el jugador está en turno para saber si se debe
        // inyectar el código de la sala de espera o el de la ruleta.
        
        //jugador = msg.jugador;
        //idJugadorEnTurno = msg.idJugadorEnTurno;
        //informacionJugadores = msg.informacionJugadores;
        //implementar toda la lógica del mensaje! OJO QUE ACÁ SE ASUME QUE LLEGA EN JSON
        //inyectar();
    };

    websocket.onerror = function(evt) {
        //$("#mensajes").append("<br>Ha ocurrido un error y no se ha podido conectar el WS :,v<br>");
        console.log("oho!.. error:" + evt.data);
    };

    websocket.onclose = function(evt) {
        console.log(evt);
    };
}

function tomarDatosJugador() {
  var nick = $("#nickname").val();
  var edad_jugador = $("#edad").val();
  var sexo = $("#genero").val();
  var puntaje_jugador = $("#puntos").val();
  var respuesta = $("#respuesta").val();

  Jugador = {
    id: 0,
    nickname: nick,
    edad: edad_jugador,
    genero: sexo,
    puntaje: 0,
    idSeleccion: 0
  }
  dataToSend = JSON.stringify(Jugador);
  return dataToSend;
};

function tomarDatosPregunta() {
  var id_pregunta = $("#id_pregunta").val();
  var categoria_pregunta = $("#categoria").val();
  var enunciado_pregunta = $("#enunciado").val();
  var opciones_respuesta = $("#opciones_respuesta").val();

  Pregunta = {
    id: id_pregunta,
    categoria: categoria_pregunta,
    enunciado: enunciado_pregunta,
    opcionesRespuesta: opciones_respuesta
  }
  dataToSend = JSON.stringify(Pregunta);
  return dataToSend;
};


function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

$(document).ready(function () {
  $('#enviar').click(function () {
    dataToSend = tomarDatosJugador()
    //ocultarElementos();
    //vistaJugador()
    $.ajax({
      url: "RecepcionDatos",
      contentType: "application/json; charset=UTF-8",
      dataType: "JSON",
      type: "POST",
      data: dataToSend,
      success: function (result) {
        alert("Registro Exitoso");
      },
    });
    sleep(1000);
    connectWS();
    document.getElementById("datos").style.display = "none";
    document.getElementById("salaEspera").style.display = "inline";
    document.getElementById("titulo").innerText = "Bienvenido a la sala de espera...";
    document.getElementById("texto").innerText = "Esperando a que se unan mas jugadores";
    // Inyectar aquí codigo html para que el jugador sepa que está esperando a
    // que se conecten mas jugadores.
  });
});

